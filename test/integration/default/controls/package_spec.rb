# frozen_string_literal: true

control 'chromium-package-install-pkg-installed' do
  title 'should be installed'

  describe package('chromium') do
    it { should be_installed }
  end
end

# frozen_string_literal: true

control 'chromium-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'chromium-config-file-bin-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/bin') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

profiles = %w[default work]
profiles.each do |profile|
  control "chromium-config-file-profile-#{profile}-auser-managed" do
    title 'should exist'

    describe file("/home/auser/bin/#{profile}-chromium") do
      it { should be_file }
      it { should be_owned_by 'auser' }
      it { should be_grouped_into 'auser' }
      its('mode') { should cmp '0755' }
      its('content') { should include('#  Your changes will be overwritten.') }
      its('content') do
        should include("$BROWSER --args --profile-directory=\"#{profile}\"")
      end
    end
  end
end

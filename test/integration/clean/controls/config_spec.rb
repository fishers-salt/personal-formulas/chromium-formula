# frozen_string_literal: true

profiles = %w[default work]
profiles.each do |profile|
  control "chromium-config-clean-profile-#{profile}-auser-absent" do
    title 'should not exist'

    describe file("/home/auser/bin/#{profile}-chromium") do
      it { should_not exist }
    end
  end
end

# frozen_string_literal: true

control 'chromium-package-clean-pkg-removed' do
  title 'should not be installed'

  describe package('chromium') do
    it { should_not be_installed }
  end
end
